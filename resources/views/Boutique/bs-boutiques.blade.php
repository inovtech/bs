<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/db-all-listing.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Oct 2020 10:50:02 GMT -->
<head>
	<title>World Best Local Directory Website template</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="<?php echo url('/'); ?>/admin/images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="<?php echo url('/'); ?>/admin/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="<?php echo url('/'); ?>/admin/css/materialize.css" rel="stylesheet">
	<link href="<?php echo url('/'); ?>/admin/css/style.css" rel="stylesheet">
	<link href="<?php echo url('/'); ?>/admin/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="<?php echo url('/'); ?>/admin/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>

	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	@include('boutique.layout.bs-header')
	<!--DASHBOARD-->
	<section>
		<div class="tz">
			<!--LEFT SECTION-->
			@include('Boutique.layout.bs-menulatteral')
			<!--CENTER SECTION-->
			<div class="tz-2">
				<div class="tz-2-com tz-2-main">
					<h4>Manage Listings</h4>
					<div class="db-list-com tz-db-table">
						<div class="ds-boar-title">
							<h2>Listings</h2>
							<p>All the Lorem Ipsum generators on the All the Lorem Ipsum generators on the</p>
						</div>
						<table class="responsive-table bordered">
							<thead>
								<tr>
									<th>Listing Name</th>
									<th>Date</th>
									<th>Rating</th>
									<th>Views</th>
									<th>Status</th>
									<th>Edit</th>
									<th>Delete</th>
									<th>Preview</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Taj Luxury Hotel</td>
									<td>12 Jan 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">76</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>National Auto Care</td>
									<td>28 Feb 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">12</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Pearl Perfumes</td>
									<td>04 Mar 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">232</span></td>
									<td><span class="db-list-ststus-na">Inactive</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Goman Travels</td>
									<td>16 Mar 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">432</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>William Watt Electricals</td>
									<td>05 Apr 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">116</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Jb Montesari School</td>
									<td>14 Apr 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">553</span></td>
									<td><span class="db-list-ststus-na">Inactive</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Ramsey Wine Corner</td>
									<td>18 Apr 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">324</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Royal Real Estates</td>
									<td>02 May 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">876</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Colors Car Services</td>
									<td>07 May 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">65</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Hotel Green Woods</td>
									<td>09 May 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">89</span></td>
									<td><span class="db-list-ststus-na">Inactive</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Hotel Aqua</td>
									<td>21 Jun 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">54</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Taj Luxury Hotel</td>
									<td>12 Jan 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">76</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>National Auto Care</td>
									<td>28 Feb 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">12</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Pearl Perfumes</td>
									<td>04 Mar 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">232</span></td>
									<td><span class="db-list-ststus-na">Inactive</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Goman Travels</td>
									<td>16 Mar 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">432</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>William Watt Electricals</td>
									<td>05 Apr 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">116</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Jb Montesari School</td>
									<td>14 Apr 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">553</span></td>
									<td><span class="db-list-ststus-na">Inactive</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Ramsey Wine Corner</td>
									<td>18 Apr 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">324</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Royal Real Estates</td>
									<td>02 May 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">876</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Colors Car Services</td>
									<td>07 May 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">65</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Hotel Green Woods</td>
									<td>09 May 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">89</span></td>
									<td><span class="db-list-ststus-na">Inactive</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
								<tr>
									<td>Hotel Aqua</td>
									<td>21 Jun 2019</td>
									<td><span class="db-list-rat">4.2</span></td>
									<td><span class="db-list-rat">54</span></td>
									<td><span class="db-list-ststus">Active</span></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Edit</a></td>
									<td><a href="db-listing-edit.html" class="db-list-edit">Delete</a></td>
									<td><a href="listing-details.html" class="db-list-edit" target="_blank"><i class="fa fa-eye"></i></a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!--RIGHT SECTION-->
			<div class="tz-3">
				<h4>Notifications(18)</h4>
				<ul>
					<li>
						<a href="#!"> <img src="<?php echo url('/'); ?>/admin/images/icon/dbr1.jpg" alt="" />
							<h5>Joseph, write a review</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="#!"> <img src="<?php echo url('/'); ?>/admin/images/icon/dbr2.jpg" alt="" />
							<h5>14 New Messages</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="#!"> <img src="<?php echo url('/'); ?>/admin/images/icon/dbr3.jpg" alt="" />
							<h5>Ads expairy soon</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="#!"> <img src="<?php echo url('/'); ?>/admin/images/icon/dbr4.jpg" alt="" />
							<h5>Post free ads - today only</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="#!"> <img src="<?php echo url('/'); ?>/admin/images/icon/dbr5.jpg" alt="" />
							<h5>listing limit increase</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="#!"> <img src="<?php echo url('/'); ?>/admin/images/icon/dbr6.jpg" alt="" />
							<h5>mobile app launch</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="#!"> <img src="<?php echo url('/'); ?>/admin/images/icon/dbr7.jpg" alt="" />
							<h5>Setting Updated</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
					<li>
						<a href="#!"> <img src="<?php echo url('/'); ?>/admin/images/icon/dbr8.jpg" alt="" />
							<h5>Increase listing viewers</h5>
							<p>All the Lorem Ipsum generators on the</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</section>
	<!--END DASHBOARD-->
	<!--MOBILE APP-->
	<section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="<?php echo url('/'); ?>/admin/images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="#"><img src="<?php echo url('/'); ?>/admin/images/android.png" alt="" /> </a>
					<a href="#"><img src="<?php echo url('/'); ?>/admin/images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section>
	<!--FOOTER SECTION-->
	@include('Boutique.layout.bs-footer')
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
						<form method="post" class="form-horizontal">
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
	<script src="<?php echo url('/'); ?>/admin/js/jquery.min.js"></script>
	<script src="<?php echo url('/'); ?>/admin/js/bootstrap.js" type="text/javascript"></script>
	<script src="<?php echo url('/'); ?>/admin/js/materialize.min.js" type="text/javascript"></script>
	<script src="<?php echo url('/'); ?>/admin/js/custom.js"></script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/db-all-listing.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Oct 2020 10:50:02 GMT -->
</html>