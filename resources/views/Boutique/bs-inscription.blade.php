<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Oct 2020 10:50:01 GMT -->
<head>
	<title>boutique senegal</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="<?php echo url('/'); ?>/images/icon.png" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="<?php echo url('/'); ?>/admin/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="<?php echo url('/'); ?>/admin/css/materialize.css" rel="stylesheet">
	<link href="<?php echo url('/'); ?>/admin/css/style.css" rel="stylesheet">
	<link href="<?php echo url('/'); ?>/admin/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="<?php echo url('/'); ?>/admin/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="<?php echo url('/'); ?>/admin/js/html5shiv.js"></script>
	<script src="<?php echo url('/'); ?>/admin/js/respond.min.js"></script>
	<![endif]-->
</head>

<body data-ng-app="">
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	@include('Boutique.layout.bs-header')

	<section class="tz-register">
			<div class="log-in-pop">
				<div class="log-in-pop-left">
					<h1>Boujour.... <span></span></h1>
					<p>Vous n'avez pas de compte? Créez votre compte. Ça prend moins d'une minute</p>
					<h4>Connectez-vous avec les médias sociaux</h4>
					<ul>
						<li><a href="#"><i class="fa fa-facebook"></i> Facebook</a>
						</li>
						<li><a href="#"><i class="fa fa-google"></i> Google+</a>
						</li>
						<li><a href="#"><i class="fa fa-twitter"></i> Twitter</a>
						</li>
					</ul>
				</div>
				<div class="log-in-pop-right">
					<a href="#" class="pop-close" data-dismiss="modal"><img src="<?php echo url('/'); ?>/admin/images/cancel.html" alt="" />
					</a>
					<h4>Créer un compte</h4>
					<p>Vous n'avez pas de compte? Créez votre compte. Ça prend moins d'une minute</p>
					<form class="s12">
						<div>
							<div class="input-field s12">
								<input type="text" data-ng-model="name1" class="validate">
								<label>User name</label>
							</div>
						</div>
						<div>
							<div class="input-field s12">
								<input type="email" class="validate">
								<label>Email id</label>
							</div>
						</div>
						<div>
							<div class="input-field s12">
								<input type="password" class="validate">
								<label>Password</label>
							</div>
						</div>
						<div>
							<div class="input-field s12">
								<input type="password" class="validate">
								<label>Confirm password</label>
							</div>
						</div>
						<div>
							<div class="input-field s4">
								<input type="submit" value="Register" class="waves-effect waves-light log-in-btn"> </div>
						</div>
						<div>
							<div class="input-field s12"> <a href="{{url('login')}}">Êtes-vous déjà un compte? S'identifier</a> </div>
						</div>
					</form>
				</div>
			</div>
	</section>
	<!--MOBILE APP-->
	<!--FOOTER SECTION-->
	@include('Boutique.layout.bs-footer')
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
	<script src="<?php echo url('/'); ?>/admin/js/jquery.min.js"></script>
	<script src="<?php echo url('/'); ?>/admin/js/angular.min.js"></script>
	<script src="<?php echo url('/'); ?>/admin/js/bootstrap.js" type="text/javascript"></script>
	<script src="<?php echo url('/'); ?>/admin/js/materialize.min.js" type="text/javascript"></script>
	<script src="<?php echo url('/'); ?>/admin/js/custom.js"></script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Oct 2020 10:50:01 GMT -->
</html>