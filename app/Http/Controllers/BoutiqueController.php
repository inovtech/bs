<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BoutiqueController extends Controller
{

    //login
    public function login()
    {
        return view('Boutique.bs-login');
    }

    //inscription
    public function inscription()
    {
        return view('Boutique.bs-inscription');
    }

    //dashboardBoutique
    public function dashboardboutique()
    {
        return view('Boutique.bs-dashboard');
    }
    //les boutiques
    public function boutiques()
        {
            return view('Boutique.bs-boutiques');
        }
    
    //ajoutboutique
     public function Ajoutboutique()
        {
            return view('Boutique.bs-AjoutBoutique');
        }
    //Messageboutique
     public function Messageboutique()
        {
            return view('Boutique.bs-message');
        }

    //commentaire
     public function Commentaireboutique()
        {
            return view('Boutique.bs-commentaire');
        }
        //Editprofil
     public function monprofil()
        {
            return view('Boutique.bs-mon-profil');
        }
    //Editprofil
     public function editprofil()
        {
            return view('Boutique.bs-edit-MonProfil');
        }
        
    //reglage
     public function reglage()
        {
            return view('Boutique.bs-reglage');
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
