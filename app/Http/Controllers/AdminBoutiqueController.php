<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminBoutiqueController extends Controller
{
    public function listeboutique()
    {
        return view('admin.listeboutique');
    }
    public function ajoutboutique()
    {
        return view('admin.ajoutboutique');
    }
}
