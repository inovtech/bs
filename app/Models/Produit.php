<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_protuit', 'categorie_produit_id', 'photo_produit', 'description_produit', 'prix_produit', 'boutique_id',
        'commander'

    ];
}
