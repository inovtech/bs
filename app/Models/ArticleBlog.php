<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleBlog extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titre_article_blog', 'description_article_blog', 'photo_article_blog', 'categorie_article_blog_id',
        'commentaire_article_blog_id', 'auteur',
    ];
}
