<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Boutique extends Model
{
    use HasFactory;
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_boutique_id', 'categorie_boutique_id', 'nom_boutique', 'adresse_boutique', 'ville_boutique', 'telephone_boutique',
        'email_boutique', 'link_facebook_boutique', 'link_instagram_boutique', 'link_twitter_boutique', 'jour_ouvrable_boutique',
        'open_time_boutique', 'close_time_boutique', 'description_boutique', 'photo_boutique', 'galery_boutique',
        'map_url_boutique', 'pack_boutique_id', 'is_active_boutique',
    ];
}
