<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentaireBoutique extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_prenom', 'email_commentaire_boutique', 'commentaire_boutique', 'objet_commentaire_boutique', 'boutique_id',
    ];
}
