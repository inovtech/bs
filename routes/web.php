<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminBoutiqueController;
use App\Http\Controllers\BoutiqueController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/admin', function () {
    return view('admin/panel-dashboard');
});

Route::get('/admin1', function () {
return view('admin1/admin');
});

Route::get('/boutique', function () {
return view('boutique/dashboard');
});

Route::get('/dashboard', function () {
    return view('Boutique.dashboard');
});


route::get('/listeboutique',[AdminBoutiqueController::class,'listeboutique']);
// route::get('/ajoutboutique',[AdminBoutiqueController::class,'ajoutboutique']);

//dashboard-boutique
route::get('/dashboard',[BoutiqueController::class,'dashboardboutique']);

//boutiques
route::get('/listeboutique',[BoutiqueController::class,'boutiques']);

//Ajoutboutiques
route::get('/Ajoutboutique',[BoutiqueController::class,'Ajoutboutique']);

//Messageboutiques
route::get('/Messageboutique',[BoutiqueController::class,'Messageboutique']);


//commentaire
route::get('/commentaire',[BoutiqueController::class,'Messageboutique']);
//reglage
route::get('/reglage',[BoutiqueController::class,'reglage']);
//monprofil
route::get('/monprofil',[BoutiqueController::class,'monprofil']);
//editprofil
route::get('/editprofil',[BoutiqueController::class,'editprofil']);

//login
route::get('/login',[BoutiqueController::class,'login']);
//inscription
route::get('/inscription',[BoutiqueController::class,'inscription']);